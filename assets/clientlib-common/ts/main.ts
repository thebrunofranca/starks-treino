let $: any;
let path: any;
let caminho: any;

$(document).ready(function () {

  $('#btn-menu').click(function () {
    $('header').toggleClass('style-mobile');
  });

  $(window).scroll(function () {
    if (window.scrollY >= $('#tecnologias').offset().top
      // && window.scrollY >= $('#footer').offset().top
    ) {
      $('header').addClass('style-scrolled');
    }
    else {
      $('header').removeClass('style-scrolled');
    }
  });

  if ($(window).width() >= 992) {
    $.scrollify({
      section: ".scroller",
      easing: "easeOutExpo",
      scrollSpeed: 1100,
      offset: 0,
      scrollbars: false,
      setHeights: false,
      overflowScroll: true,
      updateHash: true,
    });
  }

  $('.owl-carousel').owlCarousel({
    loop: true,
    margin: 50,
    nav: true,
    center: false,
    dots: true,
    autoplay: true,
    autoplayTimeout: 1000,
    autoplayHoverPause: false,

    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      600: {
        items: 3
      },

      800: {
        items: 4
      },

      1000: {
        items: 5
      }
    }
  });
});